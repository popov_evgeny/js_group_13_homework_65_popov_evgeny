import { Component, OnInit } from '@angular/core';
import { FilmService } from '../shared/film.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-film',
  templateUrl: './new-film.component.html',
  styleUrls: ['./new-film.component.css']
})
export class NewFilmComponent implements OnInit {
  filmsFetchingSubscription!: Subscription;
  loading!: boolean;
  name = '';
  description = '';

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.filmsFetchingSubscription = this.filmService.filmsFetching.subscribe((isFetching: boolean) => {
      if (!isFetching){
        this.loading = isFetching;
      }
    })
  }

  ngOnDestroy() {
    this.filmsFetchingSubscription.unsubscribe();
  }

  onClickAddFilm() {
    this.loading = true;
    const name = this.name;
    const description = this.description;
    const body = { description, name};
    this.filmService.addFilm(body);
    this.name = '';
    this.description = '';
  }
}
