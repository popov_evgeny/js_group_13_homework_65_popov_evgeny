export class FilmModel {
  constructor(
    public id: string,
    public name: string,
    public description: string
  ) {}
}
