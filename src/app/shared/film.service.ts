import { Injectable } from '@angular/core';
import { FilmModel } from './film.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Injectable()
export class FilmService {
  private arrayFilms: FilmModel[] = [];
  someError = false;
  filmsChange = new Subject<FilmModel[]>();
  filmsFetching = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  getArr() {
    return this.arrayFilms.slice();
  }

  getFilms() {
    this.filmsFetching.next(true);
    this.someError = false;
    this.http.get<{[id:string]: FilmModel}>('https://project-f65ad-default-rtdb.firebaseio.com/films.json').pipe(map(result => {
      if (result === null){
        return [];
      }
      return Object.keys(result).map(id => {
        const film = result[id];
        return new FilmModel(id, film.name, film.description);
      })
    })).subscribe( films => {
      this.arrayFilms = films;
      this.filmsChange.next(this.arrayFilms.slice());
      this.filmsFetching.next(false);
    }, () => {
      this.filmsFetching.next(false);
      this.someError = true;
      this.filmsChange.next(this.arrayFilms.slice());
    })
  }

  addFilm(body: { name: string; description: string }) {
    this.http.post<FilmModel>('https://project-f65ad-default-rtdb.firebaseio.com/films.json', body).subscribe(() => {
      this.getFilms();
    });
    this.filmsChange.next(this.arrayFilms.slice());
  }

  deleteFilm(id: string) {
    this.http.delete(`https://project-f65ad-default-rtdb.firebaseio.com/films/${id}.json`).subscribe(() => {
      this.getFilms();
    });
    this.filmsChange.next(this.arrayFilms.slice());
  }
}
