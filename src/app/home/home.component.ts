import { Component, OnDestroy, OnInit } from '@angular/core';
import { FilmModel } from '../shared/film.model';
import { FilmService } from '../shared/film.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  arrayFilms!: FilmModel[] | undefined;
  loading!: boolean;
  deleteFilmId = '';
  someInfo = '';
  filmsChangeSubscription!: Subscription;
  filmsFetchingSubscription!: Subscription;

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.arrayFilms = this.filmService.getArr();
    this.filmsChangeSubscription = this.filmService.filmsChange.subscribe((films: FilmModel[]) => {
      this.arrayFilms = films;
      if (this.arrayFilms.length === 0) {
        this.someInfo = 'No movies added.';
      } else {
        this.someInfo = '';
      }
      if (this.filmService.someError) {
        this.someInfo = 'Sorry, there was an error.';
      }
    });
    this.filmsFetchingSubscription = this.filmService.filmsFetching.subscribe((isFetching: boolean) => {
      this.loading = isFetching;
    })
    this.filmService.getFilms();
  }

  onClickDelete(id: string) {
    this.deleteFilmId = id;
    this.filmService.deleteFilm(id);
  }

  ngOnDestroy() {
    this.filmsChangeSubscription.unsubscribe();
    this.filmsFetchingSubscription.unsubscribe();
  }
}
