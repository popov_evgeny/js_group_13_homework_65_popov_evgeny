import { Component, OnInit } from '@angular/core';
import { FilmService } from './shared/film.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{}

