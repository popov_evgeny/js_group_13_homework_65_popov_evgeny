import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBtn]'
})
export class btnDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  @HostListener('click')addClass() {
    this.renderer.addClass(this.el.nativeElement, 'btn-none');
  }
}
